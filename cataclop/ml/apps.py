from django.apps import AppConfig


class MlConfig(AppConfig):
    name = 'cataclop.ml'
